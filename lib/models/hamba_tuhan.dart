// To parse this JSON data, do
//
//     final hambaTuhan = hambaTuhanFromJson(jsonString);

import 'dart:convert';

List<HambaTuhan>? hambaTuhanFromJson(String str) =>
    List<HambaTuhan>.from(json.decode(str).map((x) => HambaTuhan.fromJson(x)));

String? hambaTuhanToJson(List<HambaTuhan> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HambaTuhan {
  HambaTuhan({
    this.id,
    this.nama,
    this.jabatan,
    this.gambar,
  });

  int? id;
  String? nama;
  String? jabatan;
  String? gambar;

  factory HambaTuhan.fromJson(Map<String, dynamic> json) => HambaTuhan(
        id: json["id"],
        nama: json["nama"],
        jabatan: json["jabatan"] == null ? "" : json["jabatan"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "jabatan": jabatan == null ? "" : jabatan,
        "gambar": gambar,
      };
}

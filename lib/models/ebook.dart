// To parse this JSON data, do
//
//     final ebook = ebookFromJson(jsonString);

import 'dart:convert';

List<Ebook> ebookFromJson(String str) => List<Ebook>.from(json.decode(str).map((x) => Ebook.fromJson(x)));

String ebookToJson(List<Ebook> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Ebook {
  Ebook({
    this.id,
    this.judul,
    this.link,
    this.gambar,
  });

  int? id;
  String? judul;
  String? link;
  String? gambar;

  factory Ebook.fromJson(Map<String, dynamic> json) => Ebook(
        id: json["id"],
        judul: json["judul"],
        link: json["link"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "judul": judul,
        "link": link,
        "gambar": gambar,
      };
}

// To parse this JSON data, do
//
//     final about = aboutFromJson(jsonString);

import 'dart:convert';

About /*?*/ aboutFromJson(String /*?*/ str) => About.fromJson(json.decode(str));

String /*?*/ aboutToJson(About /*?*/ data) => json.encode(data.toJson());

class About {
  About({
    this.id,
    this.logo,
    this.coverHome,
    this.coverLain,
    this.desc,
    this.gambar,
    this.video,
    this.alamat,
    this.nomorHp,
    this.email,
    this.jamOperasional,
    this.linkLivestreaming,
    this.gambarLivestreaming,
    this.linkLivestreaming2,
    this.gambarLivestreaming2,
    this.linkLivestreaming3,
    this.gambarLivestreaming3,
    this.linkLivestreaming4,
    this.gambarLivestreaming4,
    this.linkFacebook,
    this.linkInstagram,
    this.linkYoutube,
    this.keteranganNikah,
    this.gambarNikah,
    this.videoNikah,
    this.keteranganNikahDetil,
    this.keteranganKatekisasi,
    this.gambarKatekisasi,
    this.videoKatekisasi,
    this.keteranganKatekisasiDetil,
    this.keteranganPemuridan,
    this.gambarPemuridan,
    this.videoPemuridan,
    this.keteranganPemuridanDetil,
    this.aksesKatekisasi,
    this.aksesPemuridan,
  });

  int? id;
  String? logo;
  String? coverHome;
  String? coverLain;
  String? desc;
  String? gambar;
  String? video;
  String? alamat;
  String? nomorHp;
  String? email;
  String? jamOperasional;
  String? linkLivestreaming;
  String? gambarLivestreaming;
  String? linkLivestreaming2;
  String? gambarLivestreaming2;
  String? linkLivestreaming3;
  String? gambarLivestreaming3;
  String? linkLivestreaming4;
  String? gambarLivestreaming4;
  String? linkFacebook;
  String? linkInstagram;
  String? linkYoutube;
  String? keteranganNikah;
  String? gambarNikah;
  String? videoNikah;
  String? keteranganNikahDetil;
  String? keteranganKatekisasi;
  String? gambarKatekisasi;
  String? videoKatekisasi;
  String? keteranganKatekisasiDetil;
  String? keteranganPemuridan;
  String? gambarPemuridan;
  String? videoPemuridan;
  String? keteranganPemuridanDetil;
  String? aksesKatekisasi;
  String? aksesPemuridan;

  factory About.fromJson(Map<String, dynamic> json) => About(
        id: json["id"],
        logo: json["logo"],
        coverHome: json["cover_home"],
        coverLain: json["cover_lain"],
        desc: json["desc"],
        gambar: json["gambar"],
        video: json["video"],
        alamat: json["alamat"],
        nomorHp: json["nomor_hp"],
        email: json["email"],
        jamOperasional: json["jam_operasional"],
        linkLivestreaming: json["link_livestreaming"],
        gambarLivestreaming: json["gambar_livestreaming"],
        linkLivestreaming2: json["link_livestreaming2"],
        gambarLivestreaming2: json["gambar_livestreaming2"],
        linkLivestreaming3: json["link_livestreaming3"],
        gambarLivestreaming3: json["gambar_livestreaming3"],
        linkLivestreaming4: json["link_livestreaming4"],
        gambarLivestreaming4: json["gambar_livestreaming4"],
        linkFacebook: json["link_facebook"],
        linkInstagram: json["link_instagram"],
        linkYoutube: json["link_youtube"],
        keteranganNikah: json["keterangan_nikah"],
        gambarNikah: json["gambar_nikah"],
        videoNikah: json["video_nikah"],
        keteranganNikahDetil: json["keterangan_nikah_detil"],
        keteranganKatekisasi: json["keterangan_katekisasi"],
        gambarKatekisasi: json["gambar_katekisasi"],
        videoKatekisasi: json["video_katekisasi"],
        keteranganKatekisasiDetil: json["keterangan_katekisasi_detil"],
        keteranganPemuridan: json["keterangan_pemuridan"],
        gambarPemuridan: json["gambar_pemuridan"],
        videoPemuridan: json["video_pemuridan"],
        keteranganPemuridanDetil: json["keterangan_pemuridan_detil"],
        aksesKatekisasi: json["akses_katekisasi"],
        aksesPemuridan: json["akses_pemuridan"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "logo": logo,
        "cover_home": coverHome,
        "cover_lain": coverLain,
        "desc": desc,
        "gambar": gambar,
        "video": video,
        "alamat": alamat,
        "nomor_hp": nomorHp,
        "email": email,
        "jam_operasional": jamOperasional,
        "link_livestreaming": linkLivestreaming,
        "gambar_livestreaming": gambarLivestreaming,
        "link_livestreaming2": linkLivestreaming2,
        "gambar_livestreaming2": gambarLivestreaming2,
        "link_livestreaming3": linkLivestreaming3,
        "gambar_livestreaming3": gambarLivestreaming3,
        "link_livestreaming4": linkLivestreaming4,
        "gambar_livestreaming4": gambarLivestreaming4,
        "link_facebook": linkFacebook,
        "link_instagram": linkInstagram,
        "link_youtube": linkYoutube,
        "keterangan_nikah": keteranganNikah,
        "gambar_nikah": gambarNikah,
        "video_nikah": videoNikah,
        "keterangan_nikah_detil": keteranganNikahDetil,
        "keterangan_katekisasi": keteranganKatekisasi,
        "gambar_katekisasi": gambarKatekisasi,
        "video_katekisasi": videoKatekisasi,
        "keterangan_katekisasi_detil": keteranganKatekisasiDetil,
        "keterangan_pemuridan": keteranganPemuridan,
        "gambar_pemuridan": gambarPemuridan,
        "video_pemuridan": videoPemuridan,
        "keterangan_pemuridan_detil": keteranganPemuridanDetil,
        "akses_katekisasi": aksesKatekisasi,
        "akses_pemuridan": aksesPemuridan,
      };
}

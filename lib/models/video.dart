// To parse this JSON data, do
//
//     final video = videoFromJson(jsonString);

import 'dart:convert';

List<Video> videoFromJson(String str) => List<Video>.from(json.decode(str).map((x) => Video.fromJson(x)));

String videoToJson(List<Video> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Video {
  Video({
    this.id,
    this.idHambatuhan,
    this.custom,
    this.judul,
    this.keterangan,
    this.gambar,
    this.video,
    this.hambaTuhan,
  });

  int? id;
  int? idHambatuhan;
  String? custom;
  String? judul;
  String? keterangan;
  String? gambar;
  String? video;
  HambaTuhanV? hambaTuhan;

  factory Video.fromJson(Map<String, dynamic> json) => Video(
        id: json["id"],
        idHambatuhan: json["id_hambatuhan"],
        custom: json["custom"] == null ? null : json["custom"],
        judul: json["judul"],
        keterangan: json["keterangan"],
        gambar: json["gambar"],
        video: json["video"],
        hambaTuhan: HambaTuhanV.fromJson(json["hamba_tuhan"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_hambatuhan": idHambatuhan,
        "custom": custom == null ? null : custom,
        "judul": judul,
        "keterangan": keterangan,
        "gambar": gambar,
        "video": video,
        "hamba_tuhan": hambaTuhan!.toJson(),
      };
}

class HambaTuhanV {
  HambaTuhanV({
    this.id,
    this.nama,
    this.gambar,
  });

  int? id;
  String? nama;
  String? gambar;

  factory HambaTuhanV.fromJson(Map<String, dynamic> json) => HambaTuhanV(
        id: json["id"],
        nama: json["nama"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "gambar": gambar,
      };
}

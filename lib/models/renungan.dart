// To parse this JSON data, do
//
//     final renungan = renunganFromJson(jsonString);

import 'dart:convert';

List<Renungan>? renunganFromJson(String str) => List<Renungan>.from(json.decode(str).map((x) => Renungan.fromJson(x)));

String renunganToJson(List<Renungan> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Renungan {
  Renungan({
    this.id,
    this.idHambatuhan,
    this.custom,
    this.judul,
    this.tanggal,
    this.konten,
    this.gambar,
    this.video,
    // this.hambaTuhan,
  });

  int? id;
  int? idHambatuhan;
  String? custom;
  String? judul;
  DateTime? tanggal;
  String? konten;
  String? gambar;
  String? video;
  HambaTuhanR? hambaTuhan;

  factory Renungan.fromJson(Map<String, dynamic> json) => Renungan(
        id: json["id"],
        idHambatuhan: json["id_hambatuhan"],
        custom: json["custom"] == null ? null : json["custom"],
        judul: json["judul"],
        tanggal: DateTime.parse(json["tanggal"]),
        konten: json["konten"],
        gambar: json["gambar"],
        video: json["video"] == null ? null : json["video"],
        // hambaTuhan: HambaTuhanR.fromJson(json["hamba_tuhan"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_hambatuhan": idHambatuhan,
        "custom": custom == null ? null : custom,
        "judul": judul,
        "tanggal": "${tanggal!.year.toString().padLeft(4, '0')}-${tanggal!.month.toString().padLeft(2, '0')}-${tanggal!.day.toString().padLeft(2, '0')}",
        "konten": konten,
        "gambar": gambar,
        "video": video == null ? null : video,
        // "hamba_tuhan": hambaTuhan!.toJson(),
      };
}

class HambaTuhanR {
  HambaTuhanR({
    this.id,
    this.nama,
  });

  int? id;
  String? nama;

  factory HambaTuhanR.fromJson(Map<String, dynamic> json) => HambaTuhanR(
        id: json["id"],
        nama: json["nama"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
      };
}

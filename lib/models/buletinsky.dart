// To parse this JSON data, do
//
//     final buletinsky = buletinskyFromJson(jsonString);

import 'dart:convert';

List<Buletinsky> buletinskyFromJson(String str) =>
    List<Buletinsky>.from(json.decode(str).map((x) => Buletinsky.fromJson(x)));

String buletinskyToJson(List<Buletinsky> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Buletinsky {
  Buletinsky({
    this.id,
    this.judul,
    this.link,
    this.gambar,
  });

  int? id;
  String? judul;
  String? link;
  String? gambar;

  factory Buletinsky.fromJson(Map<String, dynamic> json) => Buletinsky(
        id: json["id"],
        judul: json["judul"],
        link: json["link"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "judul": judul,
        "link": link,
        "gambar": gambar,
      };
}

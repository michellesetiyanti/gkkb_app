// To parse this JSON data, do
//
//     final warta = wartaFromJson(jsonString);

import 'dart:convert';

List<Warta> wartaFromJson(String str) =>
    List<Warta>.from(json.decode(str).map((x) => Warta.fromJson(x)));

String wartaToJson(List<Warta> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Warta {
  Warta({
    this.id,
    this.judul,
    this.link,
    this.gambar,
  });

  int? id;
  String? judul;
  String? link;
  String? gambar;

  factory Warta.fromJson(Map<String, dynamic> json) => Warta(
        id: json["id"],
        judul: json["judul"],
        link: json["link"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "judul": judul,
        "link": link,
        "gambar": gambar,
      };
}

// To parse this JSON data, do
//
//     final galerifoto = galerifotoFromJson(jsonString);

import 'dart:convert';

List<Galerifoto> galerifotoFromJson(String str) => List<Galerifoto>.from(json.decode(str).map((x) => Galerifoto.fromJson(x)));

String galerifotoToJson(List<Galerifoto> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Galerifoto {
  Galerifoto({
    this.id,
    this.judul,
    this.tanggal,
    this.keterangan,
    this.fotoDetail,
  });

  int? id;
  String? judul;
  DateTime? tanggal;
  String? keterangan;
  List<String>? fotoDetail;

  factory Galerifoto.fromJson(Map<String, dynamic> json) => Galerifoto(
        id: json["id"],
        judul: json["judul"],
        tanggal: DateTime.parse(json["tanggal"]),
        keterangan: json["keterangan"],
        fotoDetail: json["foto_detail"].cast<String>(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "judul": judul,
        "tanggal": "${tanggal!.year.toString().padLeft(4, '0')}-${tanggal!.month.toString().padLeft(2, '0')}-${tanggal!.day.toString().padLeft(2, '0')}",
        "keterangan": keterangan,
        "foto_detail": fotoDetail,
      };
}

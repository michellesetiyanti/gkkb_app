// To parse this JSON data, do
//
//     final buletin = buletinFromJson(jsonString);

import 'dart:convert';

List<Buletin> buletinFromJson(String str) =>
    List<Buletin>.from(json.decode(str).map((x) => Buletin.fromJson(x)));

String buletinToJson(List<Buletin> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Buletin {
  Buletin({
    this.id,
    this.judul,
    this.link,
    this.gambar,
  });

  int? id;
  String? judul;
  String? link;
  String? gambar;

  factory Buletin.fromJson(Map<String, dynamic> json) => Buletin(
        id: json["id"],
        judul: json["judul"],
        link: json["link"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "judul": judul,
        "link": link,
        "gambar": gambar,
      };
}

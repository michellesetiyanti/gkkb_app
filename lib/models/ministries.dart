// To parse this JSON data, do
//
//     final ministries = ministriesFromJson(jsonString);

import 'dart:convert';

Ministries ministriesFromJson(String str) =>
    Ministries.fromJson(json.decode(str));

String ministriesToJson(Ministries data) => json.encode(data.toJson());

class Ministries {
  Ministries({
    this.departemen,
  });

  List<Departeman>? departemen;

  factory Ministries.fromJson(Map<String, dynamic> json) => Ministries(
        departemen: List<Departeman>.from(
            json["departemen"].map((x) => Departeman.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "departemen": List<dynamic>.from(departemen!.map((x) => x.toJson())),
      };
}

class Departeman {
  Departeman({
    this.id,
    this.idKategori,
    this.nama,
    this.keterangan,
    this.gambar,
  });

  int? id;
  int? idKategori;
  String? nama;
  String? keterangan;
  String? gambar;

  factory Departeman.fromJson(Map<String, dynamic> json) => Departeman(
        id: json["id"],
        idKategori: json["id_kategori"],
        nama: json["nama"],
        keterangan: json["keterangan"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_kategori": idKategori,
        "nama": nama,
        "keterangan": keterangan,
        "gambar": gambar,
      };
}

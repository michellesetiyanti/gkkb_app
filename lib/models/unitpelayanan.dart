// To parse this JSON data, do
//
//     final unitpelayanan = unitpelayananFromJson(jsonString);

import 'dart:convert';

List<Unitpelayanan> unitpelayananFromJson(String str) =>
    List<Unitpelayanan>.from(
        json.decode(str).map((x) => Unitpelayanan.fromJson(x)));

String unitpelayananToJson(List<Unitpelayanan> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Unitpelayanan {
  Unitpelayanan({
    this.id,
    this.idKategori,
    this.nama,
    this.keterangan,
    this.gambar,
  });

  int? id;
  int? idKategori;
  String? nama;
  String? keterangan;
  String? gambar;

  factory Unitpelayanan.fromJson(Map<String, dynamic> json) => Unitpelayanan(
        id: json["id"],
        idKategori: json["id_kategori"],
        nama: json["nama"],
        keterangan: json["keterangan"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_kategori": idKategori,
        "nama": nama,
        "keterangan": keterangan,
        "gambar": gambar,
      };
}

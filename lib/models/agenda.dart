// To parse this JSON data, do
//
//     final agenda = agendaFromJson(jsonString);

import 'dart:convert';

List<Agenda> agendaFromJson(String str) =>
    List<Agenda>.from(json.decode(str).map((x) => Agenda.fromJson(x)));

String agendaToJson(List<Agenda> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Agenda {
  Agenda({
    this.id,
    this.idHambatuhan,
    this.judul,
    this.tanggal,
    this.konten,
    this.gambar,
    this.video,
  });

  int? id;
  int? idHambatuhan;
  String? judul;
  DateTime? tanggal;
  String? konten;
  String? gambar;
  String? video;
  factory Agenda.fromJson(Map<String, dynamic> json) => Agenda(
        id: json["id"],
        idHambatuhan: json["id_hambatuhan"],
        judul: json["judul"],
        tanggal: DateTime.parse(json["tanggal"]),
        konten: json["konten"],
        gambar: json["gambar"],
        video: json["video"] == null ? null : json["video"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_hambatuhan": idHambatuhan,
        "judul": judul,
        "tanggal":
            "${tanggal!.year.toString().padLeft(4, '0')}-${tanggal!.month.toString().padLeft(2, '0')}-${tanggal!.day.toString().padLeft(2, '0')}",
        "konten": konten,
        "gambar": gambar,
        "video": video == null ? null : video,
      };
}

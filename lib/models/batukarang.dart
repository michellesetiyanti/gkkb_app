// To parse this JSON data, do
//
//     final batukarang = batukarangFromJson(jsonString);

import 'dart:convert';

List<Batukarang> batukarangFromJson(String str) =>
    List<Batukarang>.from(json.decode(str).map((x) => Batukarang.fromJson(x)));

String batukarangToJson(List<Batukarang> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Batukarang {
  Batukarang({
    this.id,
    this.judul,
    this.link,
    this.gambar,
  });

  int? id;
  String? judul;
  String? link;
  String? gambar;

  factory Batukarang.fromJson(Map<String, dynamic> json) => Batukarang(
        id: json["id"],
        judul: json["judul"],
        link: json["link"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "judul": judul,
        "link": link,
        "gambar": gambar,
      };
}

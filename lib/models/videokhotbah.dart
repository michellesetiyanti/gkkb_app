// To parse this JSON data, do
//
//     final videoKhotbah = videoKhotbahFromJson(jsonString);

import 'dart:convert';

List<VideoKhotbah> videoKhotbahFromJson(String str) => List<VideoKhotbah>.from(json.decode(str).map((x) => VideoKhotbah.fromJson(x)));

String videoKhotbahToJson(List<VideoKhotbah> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VideoKhotbah {
  VideoKhotbah({
    this.id,
    this.idHambatuhan,
    this.custom,
    this.judul,
    this.keterangan,
    this.gambar,
    this.video,
    this.hambaTuhan,
  });

  int? id;
  int? idHambatuhan;
  String? custom;
  String? judul;
  String? keterangan;
  String? gambar;
  String? video;
  HambaTuhanVK? hambaTuhan;

  factory VideoKhotbah.fromJson(Map<String, dynamic> json) => VideoKhotbah(
        id: json["id"],
        idHambatuhan: json["id_hambatuhan"],
        custom: json["custom"] == null ? null : json["custom"],
        judul: json["judul"],
        keterangan: json["keterangan"],
        gambar: json["gambar"],
        video: json["video"],
        hambaTuhan: HambaTuhanVK.fromJson(json["hamba_tuhan"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_hambatuhan": idHambatuhan,
        "custom": custom == null ? null : custom,
        "judul": judul,
        "keterangan": keterangan,
        "gambar": gambar,
        "video": video,
        "hamba_tuhan": hambaTuhan!.toJson(),
      };
}

class HambaTuhanVK {
  HambaTuhanVK({
    this.id,
    this.nama,
    this.gambar,
  });

  int? id;
  String? nama;
  String? gambar;

  factory HambaTuhanVK.fromJson(Map<String, dynamic> json) => HambaTuhanVK(
        id: json["id"],
        nama: json["nama"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "gambar": gambar,
      };
}

// To parse this JSON data, do
//
//     final artikel = artikelFromJson(jsonString);

import 'dart:convert';

List<Artikel> artikelFromJson(String str) =>
    List<Artikel>.from(json.decode(str).map((x) => Artikel.fromJson(x)));

String artikelToJson(List<Artikel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Artikel {
  Artikel({
    this.id,
    this.idHambatuhan,
    this.judul,
    this.tanggal,
    this.konten,
    this.gambar,
    this.video,
    this.hambaTuhan,
  });

  int? id;
  int? idHambatuhan;
  String? judul;
  DateTime? tanggal;
  String? konten;
  String? gambar;
  String? video;
  HambaTuhan? hambaTuhan;

  factory Artikel.fromJson(Map<String, dynamic> json) => Artikel(
        id: json["id"],
        idHambatuhan: json["id_hambatuhan"],
        judul: json["judul"],
        tanggal: DateTime.parse(json["tanggal"]),
        konten: json["konten"],
        gambar: json["gambar"],
        video: json["video"],
        hambaTuhan: HambaTuhan.fromJson(json["hamba_tuhan"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_hambatuhan": idHambatuhan,
        "judul": judul,
        "tanggal":
            "${tanggal!.year.toString().padLeft(4, '0')}-${tanggal!.month.toString().padLeft(2, '0')}-${tanggal!.day.toString().padLeft(2, '0')}",
        "konten": konten,
        "gambar": gambar,
        "video": video,
        "hamba_tuhan": hambaTuhan!.toJson(),
      };
}

class HambaTuhan {
  HambaTuhan({
    this.id,
    this.nama,
    this.jabatan,
    this.gambar,
  });

  int? id;
  String? nama;
  String? jabatan;
  String? gambar;

  factory HambaTuhan.fromJson(Map<String, dynamic> json) => HambaTuhan(
        id: json["id"],
        nama: json["nama"],
        jabatan: json["jabatan"],
        gambar: json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "jabatan": jabatan,
        "gambar": gambar,
      };
}

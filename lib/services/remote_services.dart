import 'dart:convert';

import 'package:gkkbpontianak/models/video.dart';
import 'package:gkkbpontianak/models/videokhotbah.dart';
import 'package:http/http.dart' as http;
import 'package:gkkbpontianak/models/about.dart';
import 'package:gkkbpontianak/models/agenda.dart';
import 'package:gkkbpontianak/models/batukarang.dart';
import 'package:gkkbpontianak/models/buletin.dart';
import 'package:gkkbpontianak/models/buletinsky.dart';
import 'package:gkkbpontianak/models/galerifoto.dart';
import 'package:gkkbpontianak/models/hamba_tuhan.dart';
import 'package:gkkbpontianak/models/renungan.dart';
import 'package:gkkbpontianak/models/ebook.dart';
import 'package:gkkbpontianak/models/warta.dart';
import 'package:gkkbpontianak/models/unitpelayanan.dart';

class RemoteServices {
  static var client = http.Client();

  static Future<About?> fetchAbout() async {
    var url = 'https://gkkbpontianak.org/api/init';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var dataabout = jsonEncode(data["about"]);
      return aboutFromJson(dataabout);
    } else {
      //show error message
      print('failed fetchAbout');
      return null;
    }
  }

  static Future<List<HambaTuhan>? /*?*/ > fetchHambaTuhan() async {
    var url = 'https://gkkbpontianak.org/api/hambatuhan';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var datahamba = jsonEncode(data["hamba"]);
      return hambaTuhanFromJson(datahamba);
    } else {
      //show error message
      print('failed fetchHambaTuhan');
      return null;
    }
  }

  static Future<List<Galerifoto>? /*?*/ > fetchGaleriFoto() async {
    var url = 'https://gkkbpontianak.org/api/galerifoto';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var datahamba = jsonEncode(data["galerifoto"]);
      return galerifotoFromJson(datahamba);
    } else {
      //show error message
      print('failed fetch Galeri Foto');
      return null;
    }
  }

  static Future<List<Renungan>? /*?*/ > fetchRenungan() async {
    var url = 'https://gkkbpontianak.org/api/renungan';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var datarenungan = jsonEncode(data["renungan"]);
      return renunganFromJson(datarenungan);
    } else {
      //show error message
      print('failed fetch Renungan');
      return null;
    }
  }

  static Future<List<Video>? /*?*/ > fetchVideo() async {
    var url = 'https://gkkbpontianak.org/api/video';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var datavideo = jsonEncode(data["video"]);
      return videoFromJson(datavideo);
    } else {
      //show error message
      print('failed fetch Video');
      return null;
    }
  }

  static Future<List<VideoKhotbah>? /*?*/ > fetchVideoKhotbah() async {
    var url = 'https://gkkbpontianak.org/api/sermon';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var datavideokhotbah = jsonEncode(data["VideoKhotbah"]);
      return videoKhotbahFromJson(datavideokhotbah);
    } else {
      //show error message
      print('failed fetch Video Khotbah');
      return null;
    }
  }

  static Future<List<Ebook>? /*?*/ > fetchEbook() async {
    var url = 'https://gkkbpontianak.org/api/ebook';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var dataebook = jsonEncode(data["ebook"]);
      return ebookFromJson(dataebook);
    } else {
      //show error message
      print('failed fetchEbook');
      return null;
    }
  }

  static Future<List<Buletin>? /*?*/ > fetchBuletin() async {
    var url = 'https://gkkbpontianak.org/api/buletin';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var databuletin = jsonEncode(data["buletin"]);
      return buletinFromJson(databuletin);
    } else {
      //show error message
      print('failed fetchBuletin');
      return null;
    }
  }

  static Future<List<Buletinsky>? /*?*/ > fetchBuletinsky() async {
    var url = 'https://gkkbpontianak.org/api/buletinsky';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var databuletin = jsonEncode(data["buletin"]);
      return buletinskyFromJson(databuletin);
    } else {
      //show error message
      print('failed fetchBuletinsky');
      return null;
    }
  }

  static Future<List<Warta>? /*?*/ > fetchWarta() async {
    var url = 'https://gkkbpontianak.org/api/warta';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var datawartajemaat = jsonEncode(data["warta_jemaat"]);
      return wartaFromJson(datawartajemaat);
    } else {
      //show error message
      print('failed fetchWarta');
      return null;
    }
  }

  static Future<List<Batukarang>?> fetchBatukarang() async {
    var url = 'https://gkkbpontianak.org/api/batukarang';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var databatukarang = jsonEncode(data["batukarang"]);
      return batukarangFromJson(databatukarang);
    } else {
      //show error message
      print('failed fetchBatukarang');
      return null;
    }
  }

  static Future<List<Agenda>?> fetchAgenda() async {
    var url = 'https://gkkbpontianak.org/api/agenda';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var dataagenda = jsonEncode(data["Agenda"]);
      return agendaFromJson(dataagenda);
    } else {
      //show error message
      print('failed fetchAgenda');
      return null;
    }
  }

  static Future<List<Unitpelayanan>? /*?*/ > fetchUnitPelayanan() async {
    var url = 'https://gkkbpontianak.org/api/1/ministries';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var dataunit = jsonEncode(data["departemen"]);
      return unitpelayananFromJson(dataunit);
    } else {
      //show error message
      print('failed fetch Unit Pelayanan');
      return null;
    }
  }

  static Future<List<Unitpelayanan>? /*?*/ > fetchUnitPelayanan2() async {
    var url = 'https://gkkbpontianak.org/api/2/ministries';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var dataunit = jsonEncode(data["departemen"]);
      return unitpelayananFromJson(dataunit);
    } else {
      //show error message
      print('failed fetch Unit Pelayanan');
      return null;
    }
  }

  static Future<void> submitDoa(String name, String nohp, String keterangan) async {
    var url = 'https://gkkbpontianak.org/api/submitdoa';

    Map<String, dynamic> databody = {
      "txtname": name,
      "txtnohp": nohp,
      "txtketerangan": keterangan,
    };
    print("databody");

    print(databody);
    var response = await client.post(Uri.parse(url), body: databody);
    print(response.body);
    // return response;

    if (response.statusCode == 200) {
      // var data = jsonDecode(response.body);

      // return response;
    } else {
      // show error message
      print('failed Submit Doa');
      return null;
    }
  }

  static Future<List<Unitpelayanan>? /*?*/ > fetchUnitPelayanan3() async {
    var url = 'https://gkkbpontianak.org/api/3/ministries';
    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var dataunit = jsonEncode(data["departemen"]);
      return unitpelayananFromJson(dataunit);
    } else {
      //show error message
      print('failed fetch Unit Pelayanan');
      return null;
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/galerifoto.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class GaleriFotoController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Galerifoto>[].obs;
  var searchedList = <Galerifoto>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchGaleriFoto();
    super.onInit();
  }

  void fetchGaleriFoto() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchGaleriFoto();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/buletin.dart';
import 'package:gkkbpontianak/services/remote_services.dart';
import 'package:flutter/material.dart';

class BuletinController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Buletin>[].obs;
  var searchedList = <Buletin>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchBuletin();
    super.onInit();
  }

  void fetchBuletin() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchBuletin();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/renungan.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class RenunganController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Renungan>[].obs;
  var searchedList = <Renungan>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchRenungan();
    super.onInit();
  }

  void fetchRenungan() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchRenungan();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/videokhotbah.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class VideoKhotbahController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <VideoKhotbah>[].obs;
  var searchedList = <VideoKhotbah>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchVideoKhotbah();
    super.onInit();
  }

  void fetchVideoKhotbah() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchVideoKhotbah();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

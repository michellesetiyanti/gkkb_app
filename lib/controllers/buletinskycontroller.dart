import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/buletinsky.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class BuletinSKYController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Buletinsky>[].obs;
  var searchedList = <Buletinsky>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchBuletinsky();
    super.onInit();
  }

  void fetchBuletinsky() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchBuletinsky();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/agenda.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class KegiatanController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Agenda>[].obs;
  var searchedList = <Agenda>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchKegiatan();
    super.onInit();
  }

  void fetchKegiatan() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchAgenda();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/unitpelayanan.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class UnitPelayanan2Controller extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Unitpelayanan>[].obs;
  var searchedList = <Unitpelayanan>[].obs;
  TextEditingController searchUnitPelayanan = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchData();
    super.onInit();
  }

  void fetchData() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchUnitPelayanan2();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.nama!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

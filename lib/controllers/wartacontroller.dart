import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/warta.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class WartajemaatController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Warta>[].obs;
  var searchedList = <Warta>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchwarta();
    super.onInit();
  }

  void fetchwarta() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchWarta();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

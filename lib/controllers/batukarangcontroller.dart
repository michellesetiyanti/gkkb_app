import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/batukarang.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class BatukarangController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Batukarang>[].obs;
  var searchedList = <Batukarang>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchbatukarang();
    super.onInit();
  }

  void fetchbatukarang() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchBatukarang();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.judul!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/agenda.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class AgendaController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <Agenda>[].obs;

  @override
  void onInit() {
    fetchAgenda();
    super.onInit();
  }

  void fetchAgenda() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchAgenda();
      if (dat != null) {
        dataList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }
}

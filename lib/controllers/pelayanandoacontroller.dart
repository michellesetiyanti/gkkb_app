import 'package:flutter/cupertino.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class PelayananDoaController extends GetxController {
  var isLoading = true.obs;
  TextEditingController name = TextEditingController();
  TextEditingController nohp = TextEditingController();
  TextEditingController keterangan = TextEditingController();
  // ignore: deprecated_member_use

  @override
  void onInit() {
    // fetchEbook();
    super.onInit();
  }

  void submitDoa() async {
    isLoading(true);
    // try {
    await RemoteServices.submitDoa(name.text, nohp.text, keterangan.text);
    // if (dat != null) {
    isLoading(false);
    // }
    // } finally {
    //   isLoading(false);
    // }
  }
}

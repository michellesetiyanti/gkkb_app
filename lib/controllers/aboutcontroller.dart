import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/about.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class AboutController extends GetxController {
  var isLoading = true.obs;
  var dataabout = About().obs;

  @override
  void onInit() {
    fetchAbout();
    super.onInit();
  }

  void fetchAbout() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchAbout();
      if (dat != null) {
        dataabout.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }
}

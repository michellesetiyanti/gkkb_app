import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:gkkbpontianak/models/hamba_tuhan.dart';
import 'package:gkkbpontianak/services/remote_services.dart';

class HambaTuhanController extends GetxController {
  var isLoading = true.obs;
  // ignore: deprecated_member_use
  var dataList = <HambaTuhan>[].obs;
  var searchedList = <HambaTuhan>[].obs;
  TextEditingController searchTextController = TextEditingController(text: "");
  String dataSearch = "";

  @override
  void onInit() {
    fetchHambaTuhan();
    super.onInit();
  }

  void fetchHambaTuhan() async {
    isLoading(true);
    try {
      var dat = await RemoteServices.fetchHambaTuhan();
      if (dat != null) {
        dataList.value = dat;
        searchedList.value = dat;
      }
    } finally {
      isLoading(false);
    }
  }

  void searchData({String search = ""}) async {
    searchedList.value = dataList
        .where(
            (data) => data.nama!.toLowerCase().contains(search.toLowerCase()))
        .toList();
  }
}

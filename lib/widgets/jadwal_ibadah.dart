import 'package:flutter/material.dart';

class JadwalIbadah extends StatelessWidget {
  const JadwalIbadah(
      {Key? key,
      required this.color,
      required this.time,
      required this.days,
      required this.info2})
      : super(key: key);

  final Color color;
  final String time;
  final String days;
  final String info = "Waktu ibadah jemaat GKKB Pontianak selama masa";
  final String info2;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 7, 25, 0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(112, 144, 176, 0.2),
              blurRadius: 20,
            ),
          ],
        ),
        child: Row(
          children: [
            SizedBox(width: 18),
            Container(
              width: 5,
              height: 80,
              // color: Color.fromARGB(255, 227, 195, 63),
              color: color,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      time,
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: Colors.grey,
                          fontSize: 12),
                    ),
                    Text(
                      days,
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    ),
                    RichText(
                      text: TextSpan(
                        text: info,
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            color: Colors.grey,
                            fontSize: 12),
                        children: [
                          TextSpan(
                            text: info2,
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                                color: Colors.black,
                                fontSize: 12),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';


class MenuButton2 extends StatelessWidget {
  const MenuButton2({
    Key? key,
    required this.title,
    // required this.aboutController,
    required this.icon,
    this.onPress,
  }) : super(key: key);

  // final AboutController aboutController;
  final String title;
  final IconData icon;
  final void Function()? onPress;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(),
      child: Column(
        children: [
          Align(
            alignment: Alignment(0, 0),
            child: Material(
              child: Center(
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(243, 243, 243, 1),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: IconButton(
                    icon: Icon(icon, size: 20),
                    color: Color.fromRGBO(63, 88, 73, 1),
                    onPressed: onPress,
                  ),
                ),
              ),
            ),
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 11,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(126, 126, 126, 1),
            ),
          )
        ],
      ),
    );
  }
}

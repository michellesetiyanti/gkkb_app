import 'package:flutter/material.dart';

class JudulKonten extends StatelessWidget {
  const JudulKonten({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 28),
      child: Text(
        title,
        style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
            fontFamily: 'Poppins',
            color: Color.fromRGBO(63, 88, 73, 1)),
      ),
    );
  }
}

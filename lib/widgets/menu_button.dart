import 'package:flutter/material.dart';

class MenuButton extends StatelessWidget {
  const MenuButton({
    Key? key,
    required this.title,
    required this.icon,
    this.dialog,
  }) : super(key: key);

  final String title;
  final IconData icon;
  final Widget? dialog;

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: 80,
      // height: 100,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        // color: Colors.red,
      ),
      child: Column(
        children: [
          Material(
            child: Center(
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(243, 243, 243, 1),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: IconButton(
                  icon: Icon(icon, size: 20),
                  color: Color.fromRGBO(63, 88, 73, 1),
                  onPressed: () {
                    showGeneralDialog(
                      barrierColor: Colors.black.withOpacity(0.5),
                      transitionBuilder: (context, a1, a2, widget) {
                        return Transform.scale(
                          scale: a1.value,
                          child: Opacity(
                            opacity: a1.value,
                            child: dialog,
                          ),
                        );
                      },
                      transitionDuration: Duration(milliseconds: 110),
                      barrierDismissible: true,
                      barrierLabel: '',
                      context: context,
                      pageBuilder: (context, animation1, animation2) {
                        return Container();
                      },
                    );
                  },
                ),
              ),
            ),
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 11,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(126, 126, 126, 1),
            ),
          )
        ],
      ),
    );
  }
}

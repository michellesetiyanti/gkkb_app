import 'package:flutter/material.dart';

class ContactInformation extends StatelessWidget {
  const ContactInformation({Key? key, required this.icon, required this.cp})
      : super(key: key);

  final IconData icon;
  final String cp;
  @override
  Widget build(BuildContext context) {
    return Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        DecoratedBox(
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 206, 206, 206),
            borderRadius: BorderRadius.circular(50),
          ),
          child: Padding(
            padding: const EdgeInsets.all(6.0),
            child: Icon(
              icon,
              size: 20,
              color: Color.fromRGBO(63, 88, 73, 1),
            ),
          ),
        ),
        SizedBox(width: 14),
        Text(
          cp,
          style: TextStyle(fontFamily: 'Poppins', fontSize: 12),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PokokDoaView extends StatefulWidget {
  // final String? image, keterangan, akses, keteranganDetail;

  const PokokDoaView({
    Key? key,
    // this.image,
    // this.keterangan,
    // this.akses,
    // this.keteranganDetail,
  }) : super(key: key);

  @override
  _PokokDoaState createState() => _PokokDoaState();
}

class _PokokDoaState extends State<PokokDoaView> {
  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10))),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          title: Text(
            'Permohonan Doa',
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(Icons.arrow_back_ios_rounded),
          ),
        ),
        body: Center(
            child: Container(
                width: 150,
                padding: const EdgeInsets.all(15),
                constraints: BoxConstraints.expand(),
                // decoration: BoxDecoration(
                //     image: DecorationImage(
                //   image: CachedNetworkImageProvider(Constants.uri + (this.widget.image as String)),
                //   fit: BoxFit.cover,
                //   colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.1), BlendMode.dstATop),
                // )),
                child: InteractiveViewer(
                  child: Column(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text(
                        "Bagi anda yang memerlukan Pelayanan Besuk, Didoakan, Konseling, dan sebagainya. Anda dapat mengisi formulir yang tertera dibawah ini.",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontFamily: 'avenir', fontSize: 16),
                      ),
                      TextField(
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Nama Anda"),
                        // controller: ,
                      ),
                      TextField(
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Nomor HP"),
                        // controller: ,
                      ),
                      TextField(
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Keterangan"),
                        // controller: ,
                      ),
                      Padding(padding: EdgeInsets.all(10)),
                      TextButton(
                          child: Text("Submit".toUpperCase(),
                              style: TextStyle(fontSize: 14)),
                          style: ButtonStyle(
                              padding: MaterialStateProperty.all<EdgeInsets>(
                                  EdgeInsets.all(15)),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.green),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.green)))),
                          onPressed: () {
                            // Get.to(() => FormPemuridanView());
                          })
                    ],
                  ),
                ))));
  }
}

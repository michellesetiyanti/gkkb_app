import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:gkkbpontianak/controllers/unitpelayanan2controller.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/models/unitpelayanan.dart';

class UnitPelayananView2 extends StatefulWidget {
  @override
  _UnitPelayanan2State createState() => _UnitPelayanan2State();
}

class _UnitPelayanan2State extends State<UnitPelayananView2> {
  final UnitPelayanan2Controller unitPelayananController =
      Get.put(UnitPelayanan2Controller());

  bool isLoad = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Unit Pelayanan',
          style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 18,
              color: Color.fromRGBO(29, 29, 29, 1)),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(
        () {
          if (unitPelayananController.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            return InteractiveViewer(
              child: Column(
                children: [
                  //search bar
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 35, 25, 17),
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color.fromRGBO(243, 243, 243, 1),
                      ),
                      child: TextField(
                        controller: unitPelayananController.searchUnitPelayanan,
                        onChanged: (value) {
                          unitPelayananController.searchData(search: value);
                        },
                        decoration: InputDecoration(
                          hintText: "Search here..",
                          hintStyle: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 12,
                              color: Color.fromRGBO(142, 142, 142, 1)),
                          prefixIcon: Icon(Icons.search),
                          prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                          border: InputBorder.none,
                        ),
                        textInputAction: TextInputAction.search,
                        // onChanged: (string) {
                        //   setState(() {});
                        // },
                      ),
                    ),
                  ),

                  //konten
                  Expanded(
                    child: Obx(
                      () => ListView.builder(
                        physics: new BouncingScrollPhysics(),
                        itemCount: unitPelayananController.searchedList.length,
                        itemBuilder: (context, index) {
                          var unitPelayanan =
                              unitPelayananController.searchedList[index];

                          return UnitPelayananCard(
                              unitPelayanan: unitPelayanan);
                        },
                      ),
                    ),
                  )
                ],
              ),
            );
        },
      ),
    );
  }
}

class UnitPelayananCard extends StatelessWidget {
  const UnitPelayananCard({
    Key? key,
    required this.unitPelayanan,
  }) : super(key: key);

  final Unitpelayanan unitPelayanan;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 17),
      child: Container(
        width: 340,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(112, 144, 176, 0.2),
              blurRadius: 20,
              blurStyle: BlurStyle.outer,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
              child: CachedNetworkImage(
                height: 158,
                width: 340,
                fit: BoxFit.cover,
                imageUrl: Constants.uri +
                    (unitPelayanan.gambar as String) +
                    Constants.uriend,
                placeholder: (context, url) =>
                    Center(child: new CircularProgressIndicator()),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 20),
              child: Column(
                // crossAxisAlignment:
                //     CrossAxisAlignment.start,
                children: [
                  Text(
                    (unitPelayanan.nama as String),
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w600,
                        fontSize: 14),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Html(
                    data: unitPelayanan.keterangan,
                    style: {
                      "body": Style(
                          fontSize: FontSize(12.0),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400),
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

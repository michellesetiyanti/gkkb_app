import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants.dart';

class KuesionerDialogBox extends StatelessWidget {
  KuesionerDialogBox({Key? key, required this.dialogCont}) : super(key: key);
  final BuildContext dialogCont;
  final Uri _url = Uri.parse(
      "https://docs.google.com/forms/d/e/1FAIpQLSdGj-aESsWe8Ck3XOXCvIqJrVur-SvwGIv_oUp1lhfkaefhsA/viewform");

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Card(
                elevation: 4,
                child: ListTile(
                  title: Text(
                    'Anak Muda GKKB',
                    style: TextStyle(fontFamily: 'Poppins'),
                  ),
                  onTap: () async {
                    if (!await launchUrl(_url,
                        mode: LaunchMode.externalApplication))
                      throw 'Could not launch $_url';
                  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}

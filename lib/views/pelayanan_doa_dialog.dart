import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/controllers/pelayanandoacontroller.dart';

class PelayananDoaDialogBox extends StatefulWidget {
  final BuildContext dialogCont;

  const PelayananDoaDialogBox({
    Key? key,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _PelayananDoaDialogBoxState createState() => _PelayananDoaDialogBoxState();
}

class _PelayananDoaDialogBoxState extends State<PelayananDoaDialogBox> {
  final PelayananDoaController pelyanandoaController =
      Get.put(PelayananDoaController());

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
                left: Constants.padding,
                top: Constants.avatarRadius + Constants.padding,
                right: Constants.padding,
                bottom: Constants.padding),
            margin: EdgeInsets.only(top: Constants.avatarRadius),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Bagi anda yang memerlukan Pelayanan Besuk, Didoakan, Konseling, dan sebagainya. Anda dapat mengisi formulir yang tertera di bawah ini.",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
                ),
                SizedBox(height: 25),
                TextField(
                  decoration: InputDecoration(
                      hintStyle: TextStyle(
                          color: Color.fromRGBO(185, 185, 185, 1),
                          fontFamily: 'Poppins',
                          fontSize: 12,
                          fontWeight: FontWeight.w400),
                      hintText: "Nama Anda"),
                  controller: pelyanandoaController.name,
                ),
                TextField(
                  decoration: InputDecoration(
                      hintStyle: TextStyle(
                          color: Color.fromRGBO(185, 185, 185, 1),
                          fontFamily: 'Poppins',
                          fontSize: 12,
                          fontWeight: FontWeight.w400),
                      hintText: "Nomor HP"),
                  controller: pelyanandoaController.nohp,
                ),
                TextField(
                  decoration: InputDecoration(
                      hintStyle: TextStyle(
                          color: Color.fromRGBO(185, 185, 185, 1),
                          fontFamily: 'Poppins',
                          fontSize: 12,
                          fontWeight: FontWeight.w400),
                      hintText: "Keterangan"),
                  controller: pelyanandoaController.keterangan,
                ),
                SizedBox(height: 30),
                Container(
                  width: double.infinity,
                  height: 40,
                  child: ElevatedButton(
                      child: Text(
                        "Submit",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500),
                      ),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Color.fromRGBO(63, 88, 73, 1)),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ))),
                      onPressed: () {
                        pelyanandoaController.submitDoa();
                      }),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}

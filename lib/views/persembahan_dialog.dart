import 'package:cached_network_image/cached_network_image.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/material.dart';

class PersembahanDialogBox extends StatefulWidget {
  final BuildContext dialogCont;

  const PersembahanDialogBox({
    Key? key,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _PersembahanDialogBoxState createState() => _PersembahanDialogBoxState();
}

class _PersembahanDialogBoxState extends State<PersembahanDialogBox> {
  String albumName = 'Media';
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      child: contentBox(context),
    );
  }

  void _saveImage() async {
    setState(() {
      loading = true;
    });
    String path = 'https://gkkbpontianak.org/images/1613612969_persembahan.jpg';
    GallerySaver.saveImage(path, albumName: albumName)
        .then((value) => setState(() {
              loading = false;
              Get.snackbar("Image is saved", "Check your gallery",
                  snackPosition: SnackPosition.TOP);

              Navigator.pop(widget.dialogCont);
            }));
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: 50,
              right: Constants.padding,
              bottom: Constants.padding),
          child: loading == true
              ? Container(
                  child: Center(
                      child: Column(
                    children: [
                      new CircularProgressIndicator(),
                      SizedBox(
                        height: 22,
                      ),
                      Text(
                        "Downloading Image, Please Wait!",
                        style: TextStyle(fontFamily: 'Poppins'),
                      ),
                    ],
                  )),
                  height: 80,
                )
              : Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      child: ClipRRect(
                        child: CachedNetworkImage(
                          imageUrl:
                              "https://gkkbpontianak.org/images/1613612969_persembahan.jpg",
                          placeholder: (context, url) => new Center(
                              child: new CircularProgressIndicator()),
                          errorWidget: (context, url, error) =>
                              new Icon(Icons.error),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      width: double.infinity,
                      height: 40,
                      child: ElevatedButton(
                        onPressed: () {
                          _saveImage();
                        },
                        child: Text(
                          "Download Image",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w500),
                        ),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color.fromRGBO(63, 88, 73, 1)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ))),
                      ),
                    ),
                    SizedBox(height: 10),
                  ],
                ),
        ),
      ],
    );
  }
}

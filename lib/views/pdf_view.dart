import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class PdfApp extends StatefulWidget {
  final String link;

  PdfApp({Key? key, required this.link}) : super(key: key);

  @override
  _PdfAppState createState() => _PdfAppState();
}

class _PdfAppState extends State<PdfApp> {
  // bool _isLoading = true;

  @override
  void initState() {
    // loadDocument();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  // loadDocument() async {
  //   setState(() => _isLoading = false);
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Get.back();
        return false;
      },
      child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
        return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.black),
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: Text(
              'Artikel',
              style: TextStyle(
                  color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
            ),
            leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back_ios_rounded),
            ),
          ),
          body: Container(
            color: Colors.red,
            // height: 500,
            child: PDFViewerFromUrl(
              url: "https://gkkbpontianak.org/" + (widget.link.toString()),
            ),
          ),
        );
      }),
    );
  }
}

class PDFViewerFromUrl extends StatelessWidget {
  const PDFViewerFromUrl({Key? key, required this.url}) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const PDF().fromUrl(
        url,
        placeholder: (double progress) => Center(child: Text('$progress %')),
        errorWidget: (dynamic error) => Center(child: Text(error.toString())),
      ),
    );
  }
}

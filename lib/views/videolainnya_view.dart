import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/controllers/videokhotbahcontroller.dart';
import 'package:gkkbpontianak/models/videokhotbah.dart';
import 'package:gkkbpontianak/views/streaming_view.dart';

class VideoLainnyaView extends StatefulWidget {
  @override
  _VideoLainnyaState createState() => _VideoLainnyaState();
}

class _VideoLainnyaState extends State<VideoLainnyaView> {
  final VideoKhotbahController videokhotbahController =
      Get.put(VideoKhotbahController());

  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Video Lainnya',
          style: TextStyle(
              color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(() {
        if (videokhotbahController.isLoading.value)
          return Center(child: CircularProgressIndicator());
        else
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(25, 35, 25, 17),
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color.fromRGBO(243, 243, 243, 1),
                  ),
                  child: TextField(
                    controller: videokhotbahController.searchTextController,
                    onChanged: (value) {
                      videokhotbahController.searchData(search: value);
                    },
                    textInputAction: TextInputAction.search,
                    decoration: InputDecoration(
                      hintText: "Search here..",
                      hintStyle: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 12,
                          color: Color.fromRGBO(142, 142, 142, 1)),
                      prefixIcon: Icon(Icons.search),
                      prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: ListView.builder(
                physics: new BouncingScrollPhysics(),
                itemCount: videokhotbahController.searchedList.length,
                itemBuilder: (context, index) {
                  var videokhotbah = videokhotbahController.searchedList[index];

                  return VideoLainnyaCard(videokhotbah: videokhotbah);
                },
              ))
            ],
          );
      }),
    );
  }
}

class VideoLainnyaCard extends StatelessWidget {
  const VideoLainnyaCard({
    Key? key,
    required this.videokhotbah,
  }) : super(key: key);

  final VideoKhotbah videokhotbah;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: 25, vertical: 17),
      child: Container(
        width: 340,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(112, 144, 176, 0.2),
              blurRadius: 20,
              blurStyle: BlurStyle.outer,
            ),
          ],
        ),
        child: InkWell(
          onTap: () {
            Get.to(() => VideoApp(
                  link: videokhotbah.video as String,
                ));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
                child: CachedNetworkImage(
                  height: 158,
                  width: 340,
                  fit: BoxFit.cover,
                  imageUrl: Constants.uri +
                      (videokhotbah.gambar as String) +
                      Constants.uriend,
                  placeholder: (context, url) => Center(
                      child: new CircularProgressIndicator()),
                  errorWidget: (context, url, error) =>
                      Icon(Icons.error),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (videokhotbah
                          .hambaTuhan!.nama as String),
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          fontSize: 12),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 4),
                    Text(
                      (videokhotbah.judul as String),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins',
                          fontSize: 14),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                    SizedBox(height: 4),
                    Text(
                      (videokhotbah.keterangan as String),
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: Colors.grey,
                          fontSize: 12),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/controllers/artikelcontroller.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/models/ebook.dart';
import 'package:gkkbpontianak/views/pdf_view.dart';

class ArtikelView extends StatefulWidget {
  @override
  _ArtikelState createState() => _ArtikelState();
}

class _ArtikelState extends State<ArtikelView> {
  final ArtikelController artikelController = Get.put(ArtikelController());

  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Artikel',
          style: TextStyle(
              color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(() {
        if (artikelController.isLoading.value)
          return Center(child: CircularProgressIndicator());
        else
          return Column(
            children: [
              //search bar
              Padding(
                padding: const EdgeInsets.fromLTRB(25, 35, 25, 0),
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color.fromRGBO(243, 243, 243, 1),
                  ),
                  child: TextField(
                    controller: artikelController.searchTextController,
                    onChanged: (value) {
                      artikelController.searchData(search: value);
                    },
                    textInputAction: TextInputAction.search,
                    decoration: InputDecoration(
                      hintText: "Search here..",
                      hintStyle: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 12,
                          color: Color.fromRGBO(142, 142, 142, 1)),
                      prefixIcon: Icon(Icons.search),
                      prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),

              //konten
              Expanded(
                child: ListView.builder(
                  physics: new BouncingScrollPhysics(),
                  itemCount: artikelController.searchedList.length,
                  itemBuilder: (context, index) {
                    var artikel = artikelController.searchedList[index];

                    return ArtikelCard(
                        artikelController: artikelController, artikel: artikel);
                  },
                ),
              )
            ],
          );
      }),
    );
  }
}

class ArtikelCard extends StatelessWidget {
  const ArtikelCard({
    Key? key,
    required this.artikelController,
    required this.artikel,
  }) : super(key: key);

  final ArtikelController artikelController;
  final Ebook artikel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 17),
      child: InkWell(
        onTap: () {
          Get.to(
            () => PdfApp(
                link: artikel.link.toString(),
                ),
          );
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          elevation: 0,
          color: Colors.white,
          shadowColor: Color.fromRGBO(112, 144, 176, 1),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: CachedNetworkImage(
                    height: 117,
                    width: 87,
                    fit: BoxFit.cover,
                    imageUrl: Constants.uri +
                        (artikel.gambar as String) +
                        Constants.uriend,
                    placeholder: (context, url) =>
                        Center(child: new CircularProgressIndicator()),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 20, 20, 20),
                  child: Text(
                    (artikel.judul as String),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(63, 88, 73, 1),
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

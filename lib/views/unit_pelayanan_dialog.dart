import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gkkbpontianak/views/unit_pelayanan_view.dart';
import 'package:gkkbpontianak/views/unit_pelayanan_view2.dart';
import 'package:gkkbpontianak/views/unit_pelayanan_view3.dart';

class UnitPelayananDialogBox extends StatefulWidget {
  final BuildContext dialogCont;

  const UnitPelayananDialogBox({
    Key? key,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _UnitPelayananDialogBoxState createState() => _UnitPelayananDialogBoxState();
}

class _UnitPelayananDialogBoxState extends State<UnitPelayananDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              //departemen
              Card(
                elevation: 4,
                child: ListTile(
                  title: Text(
                    'Departemen & Usia Khusus',
                    style: TextStyle(fontFamily: 'Poppins'),
                  ),
                  onTap: () {
                    Navigator.pop(widget.dialogCont);
                    Get.to(() => UnitPelayananView());
                  },
                ),
              ),

              //pos pi
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'POS PI',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => UnitPelayananView2());
                    },
                  )),
              //bkp
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Badan Kelengkapan Pelayanan',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => UnitPelayananView3());
                    },
                  )),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}

// ignore_for_file: unnecessary_import

import 'dart:ui';
import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/views/artikel_view.dart';
import 'package:gkkbpontianak/views/batukarang_view.dart';
import 'package:gkkbpontianak/views/buletin_view.dart';
import 'package:gkkbpontianak/views/buletinsky_view.dart';
import 'package:gkkbpontianak/views/wartajemaat_view.dart';
import 'package:get/get.dart';

class ArtikelDialogBox extends StatefulWidget {
  final BuildContext dialogCont;

  const ArtikelDialogBox({
    Key? key,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _ArtikelDialogBoxState createState() => _ArtikelDialogBoxState();
}

class _ArtikelDialogBoxState extends State<ArtikelDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Artikel',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => ArtikelView());
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Buletin Bulanan',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => BuletinView());
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Buletin SKY',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => BuletinskyView());
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Warta Jemaat Mingguan',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => WartajemaatView());
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Majalah Batu Karang',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => BatukarangView());
                    },
                  )),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}

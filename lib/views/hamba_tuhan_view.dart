import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/controllers/hambatuhancontroller.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/models/hamba_tuhan.dart';

class HambaTuhanView extends StatefulWidget {
  @override
  _HambaTuhanState createState() => _HambaTuhanState();
}

class _HambaTuhanState extends State<HambaTuhanView> {
  final HambaTuhanController hambaTuhanController =
      Get.put(HambaTuhanController());

  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Hamba Tuhan',
          style: TextStyle(
              color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(
        () {
          if (hambaTuhanController.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 35, 25, 0),
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromRGBO(243, 243, 243, 1),
                    ),
                    child: TextField(
                      controller: hambaTuhanController.searchTextController,
                      onChanged: (value) {
                        hambaTuhanController.searchData(search: value);
                      },
                      textInputAction: TextInputAction.search,
                      decoration: InputDecoration(
                        hintText: "Search here..",
                        hintStyle: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 12,
                            color: Color.fromRGBO(142, 142, 142, 1)),
                        prefixIcon: Icon(Icons.search),
                        prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 25, vertical: 35),
                    child: StaggeredGridView.countBuilder(
                      physics: new BouncingScrollPhysics(),
                      crossAxisCount: 2,
                      itemCount: hambaTuhanController.searchedList.length,
                      crossAxisSpacing: 30,
                      mainAxisSpacing: 35,
                      itemBuilder: (context, index) {
                        var hambaTuhan =
                            hambaTuhanController.searchedList[index];

                        return HambaTuhanCard(hambaTuhan: hambaTuhan);
                      },
                      staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                    ),
                  ),
                )
              ],
            );
        },
      ),
    );
  }
}

class HambaTuhanCard extends StatelessWidget {
  const HambaTuhanCard({
    Key? key,
    required this.hambaTuhan,
  }) : super(key: key);

  final HambaTuhan hambaTuhan;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      elevation: 5,
      color: Colors.white,
      shadowColor: Color.fromRGBO(112, 144, 176, 1),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8),
              topRight: Radius.circular(8),
            ),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              height: 135,
              width: 150,
              imageUrl: Constants.uri +
                  (hambaTuhan.gambar as String) +
                  Constants.uriend,
              placeholder: (context, url) =>
                  Center(child: new CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12),
            child: Text(
              (hambaTuhan.nama as String),
              style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500,
                  fontSize: 12),
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/controllers/kegiatancontroller.dart';
import 'package:gkkbpontianak/models/agenda.dart';
import 'package:gkkbpontianak/utils/utils.dart';
import 'package:gkkbpontianak/views/kegiatan_detail_view.dart';

class KegiatanView extends StatefulWidget {
  @override
  _KegiatanState createState() => _KegiatanState();
}

class _KegiatanState extends State<KegiatanView> {
  final KegiatanController kegiatanController = Get.put(KegiatanController());
  List<int> verticalData = [];
  final int increment = 10;

  bool isLoadingVertical = false;

  bool isLoad = false;

  @override
  void initState() {
    _loadMoreVertical();
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future _loadMoreVertical() async {
    // Add in an artificial delay
    // await new Future.delayed(const Duration(seconds: 2));

    // verticalData.addAll(List.generate(increment, (index) => verticalData.length + index));

    setState(() {
      isLoadingVertical = false;
    });
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Kegiatan & Agenda',
          style: TextStyle(
              color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(() {
        if (kegiatanController.isLoading.value)
          return Center(child: CircularProgressIndicator());
        else
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(25, 35, 25, 0),
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color.fromRGBO(243, 243, 243, 1),
                  ),
                  child: TextField(
                    controller: kegiatanController.searchTextController,
                    onChanged: (value) {
                      kegiatanController.searchData(search: value);
                    },
                    textInputAction: TextInputAction.search,
                    decoration: InputDecoration(
                      hintText: "Search here..",
                      hintStyle: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 12,
                          color: Color.fromRGBO(142, 142, 142, 1)),
                      prefixIcon: Icon(Icons.search),
                      prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  physics: new BouncingScrollPhysics(),
                  itemCount: kegiatanController.searchedList.length,
                  itemBuilder: ((context, index) {
                    var desc = removeAllHtmlTags(
                        kegiatanController.dataList[index].konten as String);
                    var kegiatan = kegiatanController.searchedList[index];

                    return KegiatanCard(kegiatan: kegiatan, desc: desc);
                  }),
                ),
              ),
            ],
          );
      }),
    );
  }
}

class KegiatanCard extends StatelessWidget {
  const KegiatanCard({
    Key? key,
    required this.kegiatan,
    required this.desc,
  }) : super(key: key);

  final Agenda kegiatan;
  final String desc;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 17),
      child: InkWell(
        onTap: () {
          Get.to(() => KegiatanDetailView(
                judul: kegiatan.judul,
                tanggal: kegiatan.tanggal,
                konten: kegiatan.konten,
              ));
        },
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          elevation: 0,
          color: Colors.white,
          shadowColor: Color.fromRGBO(112, 144, 176, 1),
          child: Row(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: CachedNetworkImage(
                    height: 117,
                    width: 87,
                    fit: BoxFit.cover,
                    imageUrl: Constants.uri +
                        (kegiatan.gambar as String) +
                        Constants.uriend,
                    placeholder: (context, url) =>
                        Center(child: new CircularProgressIndicator()),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 20, 20, 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        (kegiatan.judul as String),
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(63, 88, 73, 1),
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                      SizedBox(height: 3),
                      Text(
                        desc,
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 5,
                      ),
                      // Html(
                      //   data: kegiatanController
                      //       .dataList[index].konten,
                      //   shrinkWrap: true,
                      //   style: {
                      //     "body": Style(
                      //         fontFamily: 'Poppins',
                      //         fontSize: FontSize(12.0),
                      //         fontWeight: FontWeight.w400,
                      //         color: Colors.grey,
                      //         maxLines: 2,
                      //         textOverflow:
                      //             TextOverflow.ellipsis,
                      //         textAlign: TextAlign.justify),
                      //   },
                      // )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

class PemuridanView extends StatefulWidget {
  final String? image, keterangan, akses, keteranganDetail;

  const PemuridanView({
    Key? key,
    this.image,
    this.keterangan,
    this.akses,
    this.keteranganDetail,
  }) : super(key: key);

  @override
  _PemuridanState createState() => _PemuridanState();
}

class _PemuridanState extends State<PemuridanView> {
  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(color: Color.fromRGBO(29, 29, 29, 1)),
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: Text(
              'Permuridan',
              style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 18,
                  color: Color.fromRGBO(29, 29, 29, 1)),
            ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),),
        body: Center(
            child: Container(
                constraints: BoxConstraints.expand(),
                color: Colors.transparent,
                child: InteractiveViewer(
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView(
                          physics: new BouncingScrollPhysics(),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(25),
                              child: Text(
                                "Kelas Pembinaan",
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Color.fromRGBO(63, 88, 73, 1)),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 25),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color.fromRGBO(112, 144, 176, 0.2),
                                      blurRadius: 20
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18, vertical: 25),
                                  child: Html(
                                    data: this.widget.keterangan,
                                    style: {
                                      "body": Style(
                                        fontSize: FontSize(12.0),
                                        fontFamily: 'Poppins',
                                      ),
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(25.0),
                              child: Text(
                                "Pendaftaran Kelas Pembinaan",
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: Color.fromRGBO(63, 88, 73, 1),
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(25, 0, 25, 30),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color.fromRGBO(112, 144, 176, 0.2),
                                      blurRadius: 20,
                                      
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18, vertical: 25),
                                  child: Html(
                                    data: this.widget.keteranganDetail,
                                    style: {
                                      "body": Style(
                                        fontSize: FontSize(12.0),
                                        fontFamily: 'Poppins',
                                      ),
                                    },
                                  ),
                                ),
                              ),
                            ),
                            this.widget.akses == 'Y'
                                ? TextButton(
                                    child: Text("Daftar Sekarang".toUpperCase(),
                                        style: TextStyle(fontSize: 14)),
                                    style: ButtonStyle(
                                        padding: MaterialStateProperty.all<
                                            EdgeInsets>(EdgeInsets.all(15)),
                                        foregroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.green),
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                                side: BorderSide(color: Colors.green)))),
                                    onPressed: () {
                                      // Get.to(() => FormPemuridanView());
                                    })
                                : Container(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ))));
  }
}

import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/views/galery_foto_view.dart';
import 'package:gkkbpontianak/views/renungan_view.dart';
import 'package:gkkbpontianak/views/video_view.dart';
import 'package:gkkbpontianak/views/videolainnya_view.dart';
import 'package:get/get.dart';

class MediaDialogBox extends StatefulWidget {
  final BuildContext dialogCont;

  const MediaDialogBox({
    Key? key,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _MediaDialogBoxState createState() => _MediaDialogBoxState();
}

class _MediaDialogBoxState extends State<MediaDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Galeri Foto',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),

                onTap: () {
                  Navigator.pop(widget.dialogCont);
                  Get.to(() => GaleriFotoView());
                },
              )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Video Ibadah',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),

                onTap: () {
                  Navigator.pop(widget.dialogCont);
                  Get.to(() => VideoView());
                },
              )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Video Lainnya',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),

                onTap: () {
                  Navigator.pop(widget.dialogCont);
                  Get.to(() => VideoLainnyaView());
                },
              )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Renungan',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),

                onTap: () {
                  Navigator.pop(widget.dialogCont);
                  Get.to(() => RenunganView());
                },
              )),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}

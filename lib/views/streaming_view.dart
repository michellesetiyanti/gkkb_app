import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoApp extends StatefulWidget {
  final String? link;

  const VideoApp({
    Key? key,
    this.link,
  }) : super(key: key);

  @override
  _VideoAppState createState() => _VideoAppState();
}

class _VideoAppState extends State<VideoApp> {
  late YoutubePlayerController _controller;
  String? videoId;

  @override
  void initState() {
    videoId = YoutubePlayer.convertUrlToId(widget.link as String);

    _controller = YoutubePlayerController(
      initialVideoId: videoId as String,
      flags: YoutubePlayerFlags(
        autoPlay: true,
        mute: false,
      ),
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
    ]);
    return WillPopScope(
      onWillPop: () async {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
        Get.back();
        return false;
      },
      child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
        if (orientation == Orientation.landscape) {
          return SafeArea(
            child: Scaffold(
              body: youtubeHierarchy(),
            ),
          );
        } else {
          return Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(color: Color.fromRGBO(29, 29, 29, 1)),
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              title: Text(
                'Video',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 18,
                    color: Color.fromRGBO(29, 29, 29, 1)),
              ),
              leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(Icons.arrow_back_ios_rounded),
              ),
            ),
            body: youtubeHierarchy(),
          );
        }
      }),
    );
  }

  youtubeHierarchy() {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: Container(
        child: Align(
          alignment: Alignment.center,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: YoutubePlayerBuilder(
              player: YoutubePlayer(
                topActions: [BackButton()],
                // aspectRatio: 16 / 9,
                controller: _controller,
              ),
              builder: (context, player) {
                return Column(
                  children: [player],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    print('dispose');
    _controller.dispose();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.dispose();
  }
}

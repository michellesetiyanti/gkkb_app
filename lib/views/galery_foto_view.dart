import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/controllers/galerifotocontroller.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/models/galerifoto.dart';

class GaleriFotoView extends StatefulWidget {
  @override
  _GaleriFotoState createState() => _GaleriFotoState();
}

class _GaleriFotoState extends State<GaleriFotoView> {
  // TextEditingController? _textEditingController = TextEditingController();
  int pageIndex = 0;
  final controller = new PageController(initialPage: 0);
  final GaleriFotoController galeriFotoController =
      Get.put(GaleriFotoController());

  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Galeri Foto',
          style: TextStyle(
              color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(
        () {
          if (galeriFotoController.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            return Column(
              children: [
                //search
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 35, 25, 0),
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromRGBO(243, 243, 243, 1),
                    ),
                    child: TextField(
                      controller: galeriFotoController.searchTextController,
                      onChanged: (value) {
                        galeriFotoController.searchData(search: value);
                      },
                      textInputAction: TextInputAction.search,
                      decoration: InputDecoration(
                        hintText: "Search here..",
                        hintStyle: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 12,
                            color: Color.fromRGBO(142, 142, 142, 1)),
                        prefixIcon: Icon(Icons.search),
                        prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),

                Expanded(
                  child: ListView.builder(
                    physics: new BouncingScrollPhysics(),
                    itemCount: galeriFotoController.searchedList.length,
                    itemBuilder: (context, index) {
                      var gallery = galeriFotoController.searchedList[index];
                      
                      return GaleriCard(gallery: gallery);
                    },
                  ),
                )
              ],
            );
        },
      ),
    );
  }

  void showImage(data) {}
}

class GaleriCard extends StatelessWidget {
  const GaleriCard({
    Key? key,
    required this.gallery,
  }) : super(key: key);

  final Galerifoto gallery;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //judul foto
          Padding(
            padding: EdgeInsets.fromLTRB(25, 30, 25, 25),
            child: Container(
              child: Text(
                (gallery.judul ?? ''),
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                  color: Color.fromRGBO(63, 88, 73, 1),
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          ),

          //konten foto
          Container(
            height: 150,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              separatorBuilder: (context, index) =>
                  const SizedBox(),
              itemCount: gallery.fotoDetail!.length,
              itemBuilder: (context, index) {
                // return Text(gallery.fotoDetail![index]);
                return Padding(
                  padding:
                      const EdgeInsets.fromLTRB(25, 0, 0, 0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                      imageUrl: (gallery.fotoDetail![index]),
                      placeholder: (context, url) => new Center(
                          child:
                              new CircularProgressIndicator()),
                      errorWidget: (context, url, error) =>
                          new Icon(Icons.error),
                      fit: BoxFit.cover,
                      width: 280,
                      height: 167,
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

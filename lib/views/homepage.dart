import 'package:cached_network_image/cached_network_image.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:gkkbpontianak/views/kuesioner_dialog.dart';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/views/artikel_dialog.dart';
import 'package:gkkbpontianak/views/media_dialog.dart';
import 'package:gkkbpontianak/views/pelayanan_doa_dialog.dart';
import 'package:gkkbpontianak/views/pemuridan_view.dart';
import 'package:gkkbpontianak/views/persembahan_dialog.dart';
import 'package:gkkbpontianak/views/register_dialog.dart';
import 'package:gkkbpontianak/views/kegiatan_view.dart';
import 'package:gkkbpontianak/views/unit_pelayanan_dialog.dart';
import '../widgets/contact_information.dart';
import '../widgets/jadwal_ibadah.dart';
import '../widgets/judul_konten.dart';
import '../widgets/menu_button.dart';
import '../widgets/menu_button2.dart';
import 'ibadah_dialog.dart';
import 'hamba_tuhan_view.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/controllers/aboutcontroller.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  // final Uri _url = Uri.parse(
  //     "https://docs.google.com/forms/d/e/1FAIpQLSdGj-aESsWe8Ck3XOXCvIqJrVur-SvwGIv_oUp1lhfkaefhsA/viewform");
  int pageIndex = 0;
  final controller = new PageController(initialPage: 0);
  final AboutController aboutController = Get.put(AboutController());
  late String? gambarPemuridan,
      gambarKatekisasi,
      alamat,
      nomorhp,
      email,
      jamOper,
      ibadahRaya,
      glowMinistry,
      skySD,
      skyTK,
      ibadahKenaikan;
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () {
          if (aboutController.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            gambarPemuridan = aboutController.dataabout().gambarPemuridan;
          gambarKatekisasi = aboutController.dataabout().gambarKatekisasi;
          alamat = aboutController.dataabout().alamat;
          nomorhp = aboutController.dataabout().nomorHp;
          email = aboutController.dataabout().email;
          jamOper = aboutController.dataabout().jamOperasional;
          ibadahRaya = aboutController.dataabout().linkLivestreaming;
          glowMinistry = aboutController.dataabout().linkLivestreaming2;
          skySD = aboutController.dataabout().linkLivestreaming3;
          skyTK = aboutController.dataabout().linkLivestreaming4;

          return SafeArea(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  //logo
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 40, 0, 0),
                    child: Image.asset(
                      'images/gkkb_logo.png',
                      height: 50,
                      width: 50,
                    ),
                  ),
                  //gkkb pontianak
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 30, 0, 8),
                    child: Text(
                      "GKKB Pontianak",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Poppins',
                          color: Color.fromRGBO(63, 88, 73, 1)),
                    ),
                  ),
                  //alamat
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: Text(
                      alamat.toString(),
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: Color.fromRGBO(125, 125, 125, 1)),
                    ),
                  ),

                  //menu button
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 30, 20, 0),
                    // color: Colors.red,
                    child: GridView.count(
                      // crossAxisSpacing: 5,
                      mainAxisSpacing: 15,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 4,
                      children: [
                        //Registrasi Ibadah
                        MenuButton(
                            title: 'Registrasi Ibadah',
                            icon: Icons.fact_check,
                            dialog: RegisterDialogBox(dialogCont: context)),

                        //Ibadah
                        MenuButton(
                            title: 'Ibadah',
                            icon: Icons.play_arrow_rounded,
                            dialog: CustomDialogBox(
                              linkStream1: ibadahRaya,
                              linkStream2: glowMinistry,
                              linkStream3: skySD,
                              linkStream4: skyTK,
                              dialogCont: context,
                            )),

                        //Artikel
                        MenuButton(
                          title: 'Artikel',
                          icon: Icons.article_rounded,
                          dialog: ArtikelDialogBox(
                            dialogCont: context,
                          ),
                        ),

                        //Media
                        MenuButton(
                          title: 'Media',
                          icon: Icons.photo_camera_back_rounded,
                          dialog: MediaDialogBox(
                            dialogCont: context,
                          ),
                        ),

                        //Unit Pelayanan
                        MenuButton(
                          title: 'Unit Pelayanan',
                          icon: Icons.assignment,
                          dialog: UnitPelayananDialogBox(
                            dialogCont: context,
                          ),
                        ),

                        //Pembinaan
                        MenuButton2(
                          title: 'Pembinaan',
                          icon: Icons.group_rounded,
                          onPress: () {
                            Get.to(
                              () => PemuridanView(
                                image:
                                    aboutController.dataabout().gambarPemuridan,
                                keterangan: aboutController
                                    .dataabout()
                                    .keteranganPemuridan,
                                akses:
                                    aboutController.dataabout().aksesPemuridan,
                                keteranganDetail: aboutController
                                    .dataabout()
                                    .keteranganPemuridanDetil,
                              ),
                            );
                          },
                        ),

                        //kegiatan
                        MenuButton2(
                          title: 'Kegiatan',
                          icon: Icons.list_alt_rounded,
                          onPress: () {
                            Get.to(() => KegiatanView());
                          },
                        ),

                        //Pelayanan Doa
                        MenuButton(
                          title: 'Pelayanan Doa',
                          icon: FontAwesomeIcons.prayingHands,
                          dialog: PelayananDoaDialogBox(
                            dialogCont: context,
                          ),
                        ),

                        //hamba tuhan
                        MenuButton2(
                          title: 'Hamba \nTuhan',
                          icon: Icons.account_tree_rounded,
                          onPress: () => Get.to(() => HambaTuhanView()),
                        ),

                        //Persembahan
                        MenuButton(
                          title: 'Persembahan',
                          icon: Icons.qr_code_2_outlined,
                          dialog: PersembahanDialogBox(
                            dialogCont: context,
                          ),
                        ),

                        MenuButton(
                          title: 'Kuesioner',
                          icon: Icons.question_answer_rounded,
                          dialog: KuesionerDialogBox(
                            dialogCont: context,
                          ),
                        ),

                        // MenuButton2(
                        //   title: 'Kuesioner',
                        //   icon: Icons.question_answer_rounded,
                        //   onPress: () async {
                        //     if (!await launchUrl(_url,
                        //         mode: LaunchMode.externalApplication))
                        //       throw 'Could not launch $_url';
                        //   },
                        // ),
                      ],
                    ),
                  ),

                  //kegiatan
                  JudulKonten(title: "Kegiatan"),

                  //konten kegiatan
                  Container(
                    height: 158,
                    child: PageView(
                      physics: BouncingScrollPhysics(),
                      controller: controller,
                      onPageChanged: (value) {
                        setState(() {
                          pageIndex = value;
                        });
                      },
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 25),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: CachedNetworkImage(
                              imageUrl:
                                  Constants.uri + (gambarKatekisasi.toString()),
                              // imageUrl: Constants.uri + (listGambar[index]),
                              placeholder: (context, url) => new Center(
                                  child: new CircularProgressIndicator()),
                              errorWidget: (context, url, error) =>
                                  new Icon(Icons.error),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 25),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: CachedNetworkImage(
                              imageUrl:
                                  Constants.uri + (gambarPemuridan.toString()),
                              // imageUrl: Constants.uri + (listGambar[index]),
                              placeholder: (context, url) => new Center(
                                  child: new CircularProgressIndicator()),
                              errorWidget: (context, url, error) =>
                                  new Icon(Icons.error),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 18, 25, 0),
                    child: SizedBox(
                      height: 10,
                      child: ListView.separated(
                        separatorBuilder: (context, index) =>
                            SizedBox(width: 5),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: 2,
                        // itemCount: KegiatanController().dataList.length,
                        itemBuilder: ((context, index) {
                          return Container(
                            width: 10,
                            height: 10,
                            decoration: BoxDecoration(
                              color: pageIndex == index
                                  ? Color.fromRGBO(63, 88, 73, 1)
                                  : Color.fromRGBO(222, 222, 222, 1),
                              borderRadius: BorderRadius.circular(50),
                            ),
                          );
                        }),
                      ),
                    ),
                  ),

                  //jadwal ibadah
                  JudulKonten(title: "Jadwal Ibadah"),

                  //konten jadwal ibadah
                  JadwalIbadah(
                      color: Color.fromARGB(255, 227, 195, 63),
                      time: "08:00 - 16:00",
                      days: "Senin - Sabtu",
                      info2: " Pandemi Covid-19"),
                  JadwalIbadah(
                      color: Color.fromARGB(255, 176, 31, 31),
                      time: "08:00 - 13:00, 17:00 - 19:00",
                      days: "Minggu",
                      info2: " Pandemi Covid-19 dan Normal"),

                  //informasi
                  JudulKonten(title: "Informasi"),
                  //konten informasi
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 30),
                    child: Container(
                      padding: EdgeInsets.all(18),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(112, 144, 176, 0.2),
                            blurRadius: 20,
                          ),
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ContactInformation(
                              icon: Icons.phone_rounded,
                              cp: "(0561) 734487 & 0812-5683-867"),
                          SizedBox(height: 10),
                          ContactInformation(
                              icon: Icons.mail_rounded, cp: "admin@gkkb.or.id"),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

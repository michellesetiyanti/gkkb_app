import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gkkbpontianak/controllers/unitpelayanancontroller.dart';

import 'package:get/get.dart';
import 'package:gkkbpontianak/constants.dart';
import 'package:gkkbpontianak/utils/utils.dart';
import 'package:readmore/readmore.dart';

class UnitPelayananView extends StatefulWidget {
  @override
  _UnitPelayananState createState() => _UnitPelayananState();
}

class _UnitPelayananState extends State<UnitPelayananView> {
  final UnitPelayananController unitPelayananController =
      Get.put(UnitPelayananController());

  bool isLoad = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => loadData());
  }

  Future loadData() async {
    setState(() {
      isLoad = true;
    });

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Unit Pelayanan',
          style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 18,
              color: Color.fromRGBO(29, 29, 29, 1)),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: Obx(() {
        if (unitPelayananController.isLoading.value)
          return Center(child: CircularProgressIndicator());
        else
          return InteractiveViewer(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 35, 25, 20),
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromRGBO(243, 243, 243, 1),
                    ),
                    child: TextField(
                      controller: unitPelayananController.searchUnitPelayanan,
                      onChanged: (value) {
                        unitPelayananController.searchData(search: value);
                      },
                      textInputAction: TextInputAction.search,
                      decoration: InputDecoration(
                        hintText: "Search here..",
                        hintStyle: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 12,
                            color: Color.fromRGBO(142, 142, 142, 1)),
                        prefixIcon: Icon(Icons.search),
                        prefixIconColor: Color.fromRGBO(142, 142, 142, 1),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: new BouncingScrollPhysics(),
                    itemCount: unitPelayananController.searchedList.length,
                    itemBuilder: (context, index) {
                      var desc = removeAllHtmlTags(unitPelayananController
                          .searchedList[index].keterangan
                          .toString());
                      var unitPelayanan =
                          unitPelayananController.searchedList[index];

                      return Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 25,
                          vertical: 15,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(112, 144, 176, 0.2),
                                  blurRadius: 20),
                            ],
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(5),
                                      child: CachedNetworkImage(
                                        height: 59,
                                        width: 59,
                                        fit: BoxFit.cover,
                                        imageUrl: Constants.uri +
                                            (unitPelayanan.gambar as String) +
                                            Constants.uriend,
                                        placeholder: (context, url) => Center(
                                            child:
                                                new CircularProgressIndicator()),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      ),
                                    ),
                                    SizedBox(width: 14),
                                    Flexible(
                                      child: Text(
                                        (unitPelayanan.nama.toString()),
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15,
                                          color: Color.fromRGBO(63, 88, 73, 1),
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: desc == ''
                                    ? EdgeInsets.zero
                                    : const EdgeInsets.fromLTRB(18, 0, 18, 38),
                                child: ReadMoreText(
                                  desc,
                                  trimLines: 5,
                                  textAlign: TextAlign.justify,
                                  trimExpandedText: 'Show Less',
                                  trimCollapsedText: 'Read More',
                                  trimMode: TrimMode.Line,
                                  colorClickableText:
                                      Color.fromRGBO(63, 117, 255, 1),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: Colors.grey,
                                  ),
                                ),
                                // child: Html(
                                //     data: unitPelayananController
                                //         .dataList[index].keterangan,
                                //     style: {
                                //       "body": Style(
                                //           fontFamily: 'Poppins',
                                //           fontSize: FontSize(12),
                                //           color: Colors.grey,
                                //           textAlign: TextAlign.justify)
                                //     }),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          );
      }),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

class KegiatanDetailView extends StatefulWidget {
  final String? judul, konten;
  final DateTime? tanggal;

  const KegiatanDetailView({
    Key? key,
    this.judul,
    this.tanggal,
    this.konten,
  }) : super(key: key);

  @override
  _KegiatanDetailState createState() => _KegiatanDetailState();
}

class _KegiatanDetailState extends State<KegiatanDetailView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0,
        title: Text(
          widget.judul as String,
          style: TextStyle(
              color: Colors.black, fontFamily: 'Poppins', fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back_ios_rounded),
        ),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: InteractiveViewer(
          child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 35, 25, 30),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(112, 144, 176, 0.2),
                      blurRadius: 20,
                    ),
                  ],
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 18, vertical: 25),
                  child: Html(
                    data: widget.konten,
                    style: {
                      "body": Style(
                          fontSize: FontSize(14.0), fontFamily: 'Poppins'),
                    },
                  ),
                ),
              )),
        ),
      ),
    );
  }
}

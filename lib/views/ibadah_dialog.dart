import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/material.dart';
import 'streaming_view.dart';
import 'package:get/get.dart';

class CustomDialogBox extends StatefulWidget {
  final String? linkStream1, linkStream2, linkStream3, linkStream4;
  final BuildContext dialogCont;

  const CustomDialogBox({
    Key? key,
    this.linkStream1,
    required this.linkStream2,
    this.linkStream3,
    this.linkStream4,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Ibadah Raya',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => VideoApp(
                            link: widget.linkStream1,
                          ));
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Glow Ministry',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => VideoApp(
                            link: widget.linkStream2,
                          ));
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'SKY SD',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => VideoApp(
                            link: widget.linkStream3,
                          ));
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'SKY TK-PG',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => VideoApp(
                            link: widget.linkStream4,
                          ));
                    },
                  )),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}

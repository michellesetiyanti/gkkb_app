import 'package:gkkbpontianak/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gkkbpontianak/views/webview_page.dart';

class RegisterDialogBox extends StatefulWidget {
  final BuildContext dialogCont;

  const RegisterDialogBox({
    Key? key,
    required this.dialogCont,
  }) : super(key: key);

  @override
  _RegisterDialogBoxState createState() => _RegisterDialogBoxState();
}

class _RegisterDialogBoxState extends State<RegisterDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Constants.padding),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Card(
                  elevation: 4,
                  child: ListTile(
                    title: Text(
                      'Umum',
                      style: TextStyle(fontFamily: 'Poppins'),
                    ),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => WebView(
                            link: "https://registrasi.gkkbpontianak.org",
                          ));
                    },
                  )),
              Card(
                  elevation: 4,
                  child: ListTile(
                    title:
                        Text('Glow', style: TextStyle(fontFamily: 'Poppins')),
                    onTap: () {
                      Navigator.pop(widget.dialogCont);
                      Get.to(() => WebView(
                            link: "https://ibadahglow.gkkbpontianak.org",
                          ));
                    },
                  )),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(Constants.avatarRadius)),
            ),
          ),
        ),
      ],
    );
  }
}
